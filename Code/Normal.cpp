#include "Normal.h"
void Normal::setMagnitude(double newMagnitude) {
	if (newMagnitude > 0)
		magnitude = newMagnitude;
	else
		magnitude = 0;
}

double Normal::getMagnitude() {
	return magnitude;
}
