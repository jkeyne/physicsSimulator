#ifndef SPRING_H
#define SPRING_H
#include "Floor.h"
#include "Box.h"
#include "Force.h"
class Spring: public Force {
private:
	Box box;
	Floor floor;
	double x;
	double y;
	double d0;
	double k;

public:
	Box getBox() { return box; }
	Floor getFloor() { return floor; }
	double getD0() { return d0; }
	double getK() { return k; }

	void setBox(Box newBox);
	void setFloor(Floor newFloor);
	void setD0(double newD0);
	void setK(double newK);

	virtual double getMagnitude();
};
#endif
