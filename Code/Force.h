#ifndef FORCE_H
#define FORCE_H
class Force {
protected:
	double angle;
	
public:
	double getAngle() { return angle; }
	virtual bool isContact() { return true; }

	void setAngle(double newAngle);

	virtual double getMagnitude() = 0;
	double getXMagnitude();
	double getYMagnitude();
	double operator+ (Force* right);
};
#endif
