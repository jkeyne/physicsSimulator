#ifndef BOX_H
#define BOX_H
#include <list>
#include <fstream>
#include "Force.h"
#include "CSV.h"

class Box {
private:
	double width;
	double length;
	double mass;
	double x;
	double y;
	double vx;
	double vy;
	double ax;
	double ay;
	std::list<Force *> forces;
public:
	double getWidth() { return width; }
	double getLength() { return width; }
	double getMass() { return mass; }
	double getX() { return x; }
	double getY() { return y; }
	double getVx() { return vx; }
	double getVy() { return vy; }
	double getAx() { return ax; }
	double getAy() { return ay; }

	void setWidth(double newWidth);
	void setLength(double newLength);
	void setMass(double newMass);
	void setX(double newX);
	void setY(double newY);
	void setVx(double newVx);
	void setVy(double newVy);
	void setAx(double newAx);
	void setAy(double newAy);

	void applyForce(Force *force);
	void removeForce(Force *force);
	void simulate(double dt);

	friend CSV& operator << (CSV &csv, const Box &val);
};
#endif
