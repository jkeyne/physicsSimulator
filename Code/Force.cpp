#include <math.h>
#include "Force.h"

void Force::setAngle(double newAngle) {
	if (newAngle >= 0 && newAngle <= 360)
		angle=newAngle;
	else
		angle=0;
}

double Force::getXMagnitude() {
	return getMagnitude() * cos(angle*M_PI/180);
}

double Force::getYMagnitude() {
	return getMagnitude() * sin(angle*M_PI/180);
}

double Force::operator+ (Force* right) {
	double tempX;
	double tempY;
	tempX = getXMagnitude();
	tempX += right->getXMagnitude();
	tempY = getYMagnitude();
	tempY += right->getYMagnitude();

	return sqrt(pow(tempX, 2) + pow(tempY, 2));
}
