#ifndef FLOOR_H
#define FLOOR_H
class Floor {
private:
	double x1;
	double y1;
	double x2;
	double y2;
	double us;
	double uk;

public:
	double getX1() { return x1; }
	double getY1() { return y1; }
	double getX2() { return x2; }
	double getY2() { return y2; }
	double getUs() { return us; }
	double getUk() { return uk; }

	void setX1(double newX1);
	void setY1(double newY1);
	void setX2(double newX2);
	void setY2(double newY2);
	void setUs(double newUs);
	void setUk(double newUk);
};
#endif
