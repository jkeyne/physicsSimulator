#include "Box.h"
#include "Floor.h"
#include "Force.h"
#include "Friction.h"
#include "Gravity.h"
#include "Normal.h"
#include "Spring.h"
#include "CSV.h"
#include <iostream>
#include <fstream>

using namespace std;

int main() {
	double t0 = 0;
	double tf = 10;
	double dt = 0.1;
	{ // Normal force testing
		Box box;
		box.setWidth(5);
		box.setLength(5);
		box.setMass(10);
		box.setX(0);
		box.setY(0);

		Normal push;
		push.setMagnitude(1);
		push.setAngle(0);
		box.applyForce(&push);

		try {
			CSV normalCSV("normal.csv");
			normalCSV << "Box moventment when push with 1N from the left" << CSV::endrow;
			normalCSV << "t" << "x" << "y" << "vx" << "vy" << "ax" << "ay" << CSV::endrow;
			for (double t = t0; t < tf; t+=dt) {
				normalCSV << t << box << CSV::endrow;
				box.simulate(dt);
				if (t == dt*4) { // at 4 cycles, stop pushing the box
					box.removeForce(&push);
				}
			}
		} catch (CSV::OPEN_FAILED) {
			cout << "Failed to open normal.csv\n";
			return 1;
		}
	}

	{ // Gravity force testing
		Box box1;
		box1.setWidth(5);
		box1.setLength(5);
		box1.setMass(1);
		box1.setX(0);
		box1.setY(0);
		Box box2;
		box2.setWidth(5);
		box2.setLength(5);
		box2.setMass(1);
		box2.setX(100);
		box2.setY(100);

		Gravity gravity;
		gravity.setBox1(box1);
		gravity.setBox2(box2);
		box1.applyForce(&gravity);
		try {
			CSV gravityCSV("gravity.csv");
			gravityCSV << "Gravity between box and box2, box2 is stationary\n";
			gravityCSV << "t" << "x" << "y" << "vx" << "vy" << "ax" << "ay" << CSV::endrow;
			for (double t = t0; t < tf; t+=dt) {
				gravityCSV << t << box1 << CSV::endrow;
				box1.simulate(dt);
			}
		} catch (CSV::OPEN_FAILED) {
			cout << "Failed to open gravity.csv\n";
			return 2;
		}
	}

	{ // Spring force testing
		Box box1;
		box1.setWidth(5);
		box1.setLength(5);
		box1.setMass(10);
		box1.setX(0);
		box1.setY(0);

		Floor floor;
		floor.setX1(-5);
		floor.setY1(5);
		floor.setX2(-5);
		floor.setY2(-5);

		Spring spring;
		spring.setK(10);
		spring.setD0(10);
		spring.setBox(box1);
		spring.setFloor(floor);
		spring.setAngle(0);

		try {
			CSV springCSV("spring.csv");
			springCSV << "t" << "x" << "y" << "vx" << "vy" << "ax" << "ay" << CSV::endrow;
			for (double t = t0; t < tf; t += dt) {
				springCSV << t << box1 << CSV::endrow;
				box1.simulate(dt);
			}
		} catch (CSV::OPEN_FAILED) {
			cout << "Failed to open spring.csv\n";
			return 4;
		}
	}

	{ // Friction force testing
		Box box;
		box.setX(0);
		box.setY(0);
		box.setLength(2);
		box.setWidth(0);
		box.setMass(10);

		Floor floor;
		floor.setX1(-5);
		floor.setX2(5);
		floor.setY1(0);
		floor.setY2(0);
		floor.setUk(.5);
		floor.setUs(.6);

		Normal push;
		push.setAngle(0);
		push.setMagnitude(10);
		box.applyForce(&push);

		Friction friction;
		friction.setFN(push);
		friction.setSurface(floor);
		friction.setObject(box);
		box.applyForce(&friction);

		try {
			CSV frictionCSV("friction.csv");
			frictionCSV << "t" << "x" << "y" << "vx" << "vy" << "ax" << "ay" << CSV::endrow;
			for (double t = t0; t < tf; t += dt) {
				frictionCSV << t << box << CSV::endrow;
				box.simulate(dt);
			}
		} catch (CSV::OPEN_FAILED) {
			cout << "Failed to open friction.csv\n";
			return 4;
		}
	}


	return 0;
}
