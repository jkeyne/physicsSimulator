#ifndef GRAVITY_H
#define GRAVITY_H
#include "Box.h"
#include "Force.h"
class Gravity: public Force {
private:
	static double G;
	Box box1;
	Box box2;

public:
	Box getBox1() { return box1; }
	Box getBox2() { return box2; }
	virtual bool isContact() { return false; }

	static double getGravityConstant() { return G; }

	void setBox1(Box newBox);
	void setBox2(Box newBox);

	virtual double getMagnitude();
};
#endif
