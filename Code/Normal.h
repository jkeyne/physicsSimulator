#ifndef NORMAL_H
#define NORMAL_H
#include "Force.h"
class Normal: public Force {
private:
	double magnitude;

public:
	void setMagnitude(double newMagnitude);

	virtual double getMagnitude();
};
#endif
