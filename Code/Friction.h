#ifndef FRICTION_H
#define FRICTION_H
#include "Normal.h"
#include "Floor.h"
#include "Box.h"
#include "Force.h"
class Friction: public Force {
private:
	Normal fn;
	Floor surface;
	Box object;

public:
	Normal getFN() { return fn; }
	Floor getSurface() { return surface; }
	Box getObject() { return object; }

	void setFN(Normal newFN);
	void setSurface(Floor newSurface);
	void setObject(Box newObject);

	virtual double getMagnitude();
};
#endif
