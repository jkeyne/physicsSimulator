#include "Box.h"
#include "CSV.cpp"
#include <fstream>
void Box::setWidth(double newWidth) {
	if (newWidth > 0)
		width = newWidth;
	else
		width = 0;
}

void Box::setLength(double newLength) {
	if (newLength > 0)
		length = newLength;
	else
		length = 0;
}

void Box::setMass(double newMass) {
	if (newMass > 0)
		mass = newMass;
	else
		mass = 0;
}

void Box::setX(double newX) {
	x = newX;
}

void Box::setY(double newY) {
	y = newY;
}

void Box::setVx(double newVx) {
	vx = newVx;
}

void Box::setVy(double newVy) {
	vy = newVy;
}

void Box::setAx(double newAx) {
	ax = newAx;
}

void Box::setAy(double newAy) {
	ay = newAy;
}

void Box::applyForce(Force *force) {
	forces.push_back(force);
}

void Box::removeForce(Force *force) {
	forces.remove(force);
}

void Box::simulate(double dt) {
	// net F=ma
	double netFX = 0;
	double netFY = 0;
	for (auto it = forces.begin(); it != forces.end(); it++) {
		netFX+=(*it)->getXMagnitude();
		netFY+=(*it)->getYMagnitude();
		//cout << (*it)->getXMagnitude() << " " << (*it)->getYMagnitude() << endl;
	}

	ax=netFX/mass;
	ay=netFY/mass;

	// v = \int{a dt}
	// dv = a*dt
	// v ~= \sum{dv}
	double dVx = ax*dt;
	double dVy = ay*dt;
	vx+=dVx;
	vy+=dVy;

	// x = \int{vx dt}
	// dx = vx*dt
	// x ~= \sum{dx}
	double dx = vx*dt;
	double dy = vy*dt;
	x+=dx;
	y+=dy;
}

CSV& operator<< (CSV &csv, const Box &box) {
	csv << box.x << box.y << box.vx << box.vy << box.ax << box.ay;
	return csv;
}
