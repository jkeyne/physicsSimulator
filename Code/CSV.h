#ifndef CSV_H
#define CSV_H
#include <fstream>
#include <string>
#include "Box.h"
class CSV {
private:
	std::ofstream os;
	void open(std::string fName);
public:
	class OPEN_FAILED {};
	static std::string endrow;

	static const std::string SEPERATOR;

	CSV& operator << (const std::string & val);
	CSV& operator << (const char * val);
	template<class T>
	CSV& operator << (const T& val);

	void close();
	CSV(std::string fName);
	~CSV();
};
#endif
