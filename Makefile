CC=clang++
CFLAGS=-g

CLASSFILES := Code/Box.cpp Code/Floor.cpp Code/Force.cpp Code/Friction.cpp Code/Gravity.cpp Code/Normal.cpp Code/Spring.cpp 

all: $(CLASSFILES)
	${CC} ${CFLAGS} $^ Code/test.cpp -o Builds/testing && Builds/testing

clean: 
	rm Builds/testing
