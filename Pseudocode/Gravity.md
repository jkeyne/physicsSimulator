```cpp
class Gravity: Force {
	getMagnitude() {
		return G * ( (box1.getMass() * box2.getMass()) / abs((box1.getX()-box2.getX())^2 - (box1.getY()-box2.getY())^2) );
	}
};
```