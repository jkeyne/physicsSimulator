# Gravity: Force
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | G: double = 6.67x10^-11 | The gravitational constant |
\- | box1: Box | The first box |
\- | box2: Box | The second box |
\+ | getBox1(): Box | Returns box1 |
\+ | setBox1(newBox: Box): void | Sets box1 to newBox |
\+ | getBox2(): Box | Returns box2 |
\+ | setBox2(newBox: Box): void | Sets box2 to newBox |

## Notes
	* A Gravity force can only repel an object
	* Non-contact force
	* F = G((m1*m2)/(r^2))
