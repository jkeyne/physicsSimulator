# Friction: Force
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | fn: Normal | The normal force |
\- | surface: Floor | The surface with the friction |
\- | object: Box | The object that is trying to move |
\+ | getFN(): Normal | Returns fn | 
\+ | setFN(newFN: Normal): void | Sets fn to newFN | 
\+ | getSurface(): Floor | Returns surface |
\+ | setSurface(newSurface: Floor): void | Sets surface to newSurface |
\+ | getObject(): Box | Returns object |
\+ | setObject(newObject: Box): void | Sets object to newObject |

## Notes
	* Contact force
