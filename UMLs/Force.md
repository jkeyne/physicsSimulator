# Force
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | angle: double | The angle the force acts on an object (from the center of the object) | between 0 and 360 degrees inclusively
\+ | getMagnitude(): virtual double = 0 | Pure virtual function that will calculate the magnitude of the force | 
\+ | getXMagnitude(): double | Calculates and returns the magnitude along the x-axis |
\+ | getYMagnitude(): double | Calculates and returns the magnitude along the y-axis |
\+ | getAngle(): double | Returns angle | 
\+ | setAngle(newAngle: double): void | Sets angle to newAngle | newAngle must be between 0 and 360 degrees inclusively

## Notes
	* For the angle 0 degrees is positive along the x-axis (to the right), 90 degrees is positive along the y-axis (up)
