# Spring: Force
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | box: Box | This is the box that will move | 
\- | floor: Floor | This is a floor/wall that is anchored |
\- | x: double | The x-coordinate where the spring is anchored | 
\- | y: double | The y-coordinate where the spring is anchored | 
\- | d0: double | This is the distance between the objects where the spring is in its relaxed state | must be greater than 0
\- | k: double | This is the spring constant |
\+ | getBox(): Box | Returns box | 
\+ | setBox(newBox: Box): void | Sets box to newBox | 
\+ | getFloor(): Floor | Returns floor |
\+ | setFloor(newFloor: Floor): void | Sets floor to newFloors |
\+ | getD0(): double | Returns d0 | 
\+ | setD0(newD0: double): void | Sets d0 to newD0 | newD0 must be greater than 0
\+ | getK(): double | Returns k |
\+ | setK(newK: double): void | Sets k to newK |
## Notes
	* Contact force
	* F = k * d
