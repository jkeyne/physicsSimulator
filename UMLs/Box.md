# Box
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | width: double | The width of the box | must be greater than 0
\- | length: double | The length of the box | must be greater than 0
\- | mass: double | The mass of the box | must be greater than 0
\- | x: double | The x-coordinate of the center of the box | 
\- | y: double | The y-coordinate of the center of the box | 
\- | vx: double | The velocity of the box along the x-axis | 
\- | vy: double | The velocity of the box along the y-axis |
\- | ax: double | The acceleration of the box along the x-axis |
\- | ay: double | The acceleration of the box along the y-axis |
\- | forces: list<Force *> | A list of each force being applied to the box |
\+ | getWidth(): double | Returns width | 
\+ | setWidth(newWidth: double): void | Sets width to newWidth | newWidth must be greater than 0
\+ | getLength(): double | Returns length |
\+ | setLength(newLength: double): void | Sets length to newLength | newLength must be greater than 0
\+ | getMass(): double | Returns mass | 
\+ | setMass(newMass: double): void | Sets mass to newMass | newMass must be greater than 0
\+ | getX(): double | Returns x | 
\+ | setX(newX: double): void | Sets x to newX | 
\+ | getY(): double | Returns y | 
\+ | setY(newY: double): void | Sets y to newY | 
\+ | getVx(): double | Returns vx | 
\+ | setVx(newVx: double): void | Sets vx to newVx | 
\+ | getVy(): double | Returns vy |
\+ | setVy(newVy: double): void | Sets vy to newVy |
\+ | getAx(): double | Returns ax |
\+ | setAx(newAx: double): void | Sets ax to newAx |
\+ | getAy(): double | Returns ay |
\+ | setAy(newAy: double): void | Sets ay to newAy |
\+ | applyForce(force: Force): void | Adds force to forces | 
\+ | simulate(dt: double): void | Calculates the new accelerations, velocities, and the position of the box after dt seconds | dt must be greater than 0
\+ | writeToCSV(os: ostream &): void | This will take the x, y, velocity, and acceleration of the box and write it to a csv file


## Notes
		
