# Floor
Accessiblilty | Name | Description | Verification
--- | --- | --- | ---
\- | x1: double | x-coordinate of the left corner of the floor |
\- | y1: double | y-coordinate of the left corner of the floor |
\- | x2: double | x-coordinate of the right corner of the floor |
\- | y2: double | y-coordinate of the right corner of the floor |
\- | us: double | Coefficiant of static friction |
\- | uk: double | Coefficiant of kinetic friction |
\+ | getX1(): double | Returns x1 |
\+ | setX1(newX: double): void | Sets x1 to newX |
\+ | getY1(): double | Returns y1 |
\+ | setY1(newY: double): void | Sets y1 to newY |
\+ | getX2(): double | Returns x2 |
\+ | setX2(newX2: double): void | Sets x2 to newX2 |
\+ | getY2(): double | Returns y2 |
\+ | setY2(newY2: double): void | Sets y2 to newY2 |
\+ | getUs(): double | Returns us |
\+ | setUs(newUs: double): void | Sets us to newUs |
\+ | getUk(): double | Returns uk |
\+ | setUk(newUk: double): void | Sets uk to newUk |
## Notes
	This floor will not react to any forces, but there can be a normal force from the floor pushing on the box
